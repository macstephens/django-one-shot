from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm, TodoItemForm

# Create your views here.


def todo_list(request):
    items = TodoList.objects.all()
    context = {
        "todo_list": items,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": item,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoForm(instance=model_instance)

    context = {
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)
    else:
        form = TodoItemForm(instance=model_instance)

    context = {
        "form": form,
    }

    return render(request, "todos/items/edit.html", context)
